# apkutil
## Usage
* Enable root in emulator settings (Nox and MEmu only, Bluestacks is not supported)
* `apkutil pull com.igg.android.lordsmobile` - pull the Lords apk from the device, extract and decrypt the 
Assembly-CSharp.dll
* Use [dnSpy](https://github.com/0xd4d/dnSpy) to decompile, modify and recompile the dll
* `apkutil push com.igg.android.lordsmobile` - encrypt the modified dll and push the modified apk back to the device
## Notes
* Google Play version of lords is supported
* Amazon version of lords is not tested