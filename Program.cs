﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;

namespace ApkUtil
{
    internal static class Program
    {
        private static readonly byte[] Key = {0x95, 0xcc, 0x8e, 0x8f, 0x90, 0x9c, 0xb7, 0x87};
        private const string DllName = "Assembly-CSharp.dll";
        private const string ApkDllPath = @"assets/bin/Data/Managed/Assembly-CSharp.dll";

        private static SyncService _syncSvc;
        private static DeviceData _device;
        
        private static byte[] Crypt(IReadOnlyList<byte> data, IReadOnlyList<byte> key)
        {
            var result = new byte[data.Count];
            for (var i = 0; i < data.Count; i++)
            {
                result[i] = (byte)(key[i % key.Count] ^ data[i]);
            }

            return result;
        }

        private static void PullApk(string packageName)
        {
            var manager = new PackageManager(_device);
            if (manager.Packages.TryGetValue(packageName, out var remotePathToApk) == false)
            {
                Console.WriteLine($"Package {packageName} not found.");
                return;
            }
            var ver = manager.GetVersionInfo(packageName);
            var dir = Path.Combine(AppContext.BaseDirectory, packageName, ver.VersionName + "-" + ver.VersionCode);
            Console.WriteLine($"Pulling apk from {packageName} ({ver.VersionName}-{ver.VersionCode}) ...");
            Directory.CreateDirectory(dir);
            var localPathToApk = $@"{dir}\base.apk";
            using (var stream = File.Create(localPathToApk))
            {
                _syncSvc.Pull(remotePathToApk, stream, null, CancellationToken.None);
            }
            
            var archive = ZipFile.OpenRead(localPathToApk);
            var entries = archive.Entries.Where(e => e.FullName.Contains("assets/bin/Data/Managed/"));
            foreach (var entry in entries)
            {
                var filePath = Path.Combine(dir, entry.Name);
                File.Delete(filePath);
                entry.ExtractToFile(filePath);
            }

            var f = new FileInfo(Path.Combine(dir, DllName));
            var bytes = File.ReadAllBytes(f.FullName);
            File.WriteAllBytes(f.FullName, Crypt(bytes, Key));
        }

        private static void PushApk(string packageName, string version)
        {
            var manager = new PackageManager(_device);
            if (manager.Packages.TryGetValue(packageName, out var remotePathToApk) == false)
            {
                Console.WriteLine($"Package {packageName} not found.");
                return;
            }
            if (string.IsNullOrEmpty(version))
            {
                var d = new DirectoryInfo(Path.Combine(AppContext.BaseDirectory, packageName)).GetDirectories().LastOrDefault();
                if (d is null)
                {
                    Console.WriteLine("Nothing to push.");
                    return;
                }

                version = d.Name;
            }
            Console.WriteLine($"Pushing apk to {packageName} ...");
            
            var dll = Path.Combine(AppContext.BaseDirectory, packageName, version, DllName);
            var f = new FileInfo(dll);
            var bytes = File.ReadAllBytes(f.FullName);
            File.WriteAllBytes(dll + "-xored", Crypt(bytes, Key));
            
            var localPathToApk = $@"{packageName}\base.apk";
            var archive = ZipFile.Open(localPathToApk, ZipArchiveMode.Update);
            var oldEntry = archive.GetEntry(ApkDllPath);
            oldEntry?.Delete();
            archive.CreateEntryFromFile(dll +"-xored", ApkDllPath, CompressionLevel.Optimal);
            archive.Dispose();
            
            using (Stream stream = File.OpenRead(localPathToApk))
            {
                _syncSvc.Push(stream, remotePathToApk, 755, DateTime.Now, null, CancellationToken.None);
            }
        }
        
        private static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                ShowHelp();
                return;
            }

            try
            {
                _device = AdbClient.Instance.GetDevices().First();
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("No devices found.");
                return;
            }
            catch (SocketException e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            
            Console.WriteLine($"Using device '{_device.Name}' ({_device.Model}).");
            
            _syncSvc = new SyncService(
                new AdbSocket(new IPEndPoint(IPAddress.Loopback, AdbClient.AdbServerPort)), _device);
            
            switch (args[0])
            {
                case "push":
                    PushApk(args[1], args[2]);
                    break;
                case "pull":
                    PullApk(args[1]);
                    break;
                default:
                    ShowHelp();
                    return;
            }
        }
        
        private static void ShowHelp()
        {
            var exe = Path.GetFileName(AppDomain.CurrentDomain.FriendlyName);
            Console.WriteLine($"Usage: {exe} push|pull <package>");
            foreach (var device in AdbClient.Instance.GetDevices())
            {
                Console.WriteLine($"{device.Model} {device.Name} {device.State} {device.Product} ");
            }
        }
    }
}